#!/usr/bin/perl

sub isPowerOfTwo {
    my $x = shift;
    return 0 == ( $x & ($x-1) ) ? 1 : 0;
}

sub ceilToNearsetPowerOfTwo {
    my $x = shift;
    $x--;
    $x = $x | ($x >> 1);
    $x = $x | ($x >> 2);
    $x = $x | ($x >> 4);
    $x = $x | ($x >> 8);
    $x = $x | ($x >> 16);
    return ++$x;
}

my %members = ();

while( my $line = <STDIN>)  {
    chomp $line;
    
    $line =~ s/\s//g;
    my @tokens = split(/,/, $line);

    die( "Line has to have exactly 3 tokens separeted by comma." ) if scalar @tokens != 3;
    foreach( @tokens ) {
         die( "Line has to have exactly 3 tokens separeted by comma." ) if $_ eq "";
    }

    if( not exists $members{$tokens[0]} ) {
        $members{$tokens[0]}[0] = undef;
    }
    
    # * = (childsName).("Dad"/"Mom") => childsName, undef, undef
    if( $tokens[1] eq "*" ) {
        my $unknownFather = $tokens[0]."SDad";
        $members{$tokens[0]}[1] = $unknownFather;
        $members{$unknownFather} = [ $tokens[0], undef, undef ];
    }
    else {
        $members{$tokens[0]}[1] = $tokens[1];
    }

    if( $tokens[2] eq "*" ) {
        my $unknownMother = $tokens[0]."SMom";
        $members{$tokens[0]}[2] = $unknownMother;
        $members{$unknownMother} = [ $tokens[0], undef, undef ];
    }
    else {
        $members{$tokens[0]}[2] = $tokens[2];
    }

    if( $members{$tokens[0]}[1] =~ /[Aa]$/ || $members{$tokens[0]}[1] =~ /Mom$/ ) {
        my $tmp = $members{$tokens[0]}[1];
        $members{$tokens[0]}[1] = $members{$tokens[0]}[2];
        $members{$tokens[0]}[2] = $tmp;
    }

    for( my $i = 1; $i <= 2; $i++ ) {
        if( $tokens[$i] ne '*' ) {
            if( not exists $members{$tokens[$i]} ) {
                $members{$tokens[$i]} = [ $tokens[0], undef, undef ];
            }
            else {
                $members{$tokens[$i]}[0] = $tokens[0];
            }
        }
    }
}

my $numberOfMembers = keys %members;
my $rootKey = "";
my $foundRootKeyAlready = 0;

foreach my $key (keys %members) { 
    die "Malformed family tree." if $foundRootKeyAlready && not defined $members{$key}[0];
    
    if( not defined $members{$key}[0] ) {
        $rootKey = $key;
        $foundRootKeyAlready = 1;
    }
}

my @htmlTable = ("<table>");

my @queue = ($rootKey);
my $currentNumberOfColspans = ceilToNearsetPowerOfTwo( scalar keys %members );
my $lastPower = 0;
my $currentCell = "";

my $node = $rootKey;

while ( @queue ) {
    if( isPowerOfTwo( 0+@queue ) ) {
        my $continueOnMembersPrinting = 0;
        foreach( @queue ) {
             if( $_ !~ /(Mom|Dad)$/ ) { 
                 $continueOnMembersPrinting = 1;
                 last;
             }
        }
        last if not $continueOnMembersPrinting;

        $node = shift @queue;

        if( $lastPower < 1+@queue ) {
            $lastPower = 1+@queue;

            if( $node ne $rootKey ) {
                $currentCell = $currentCell.join '', "</tr>";
                push @htmlTable, $currentCell;
            }
            $currentCell = "";
            
            $currentCell = $currentCell.join '', "<tr>";
            $currentNumberOfColspans /= 2;
        }
    }
    else {
        $node = shift @queue;
    }

    my $genderColor = ( $node =~ /[Aa]$/ || $node =~ /Mom$/ ) ? "#f441ee" : "#42eef4";
    $currentCell = $currentCell.join '', "<td colspan=\"$currentNumberOfColspans\" bgcolor=\"$genderColor\">";
    if( $node =~ /((Mom)|(Dad))$/ ) {
        $currentCell = $currentCell.join '', "*</td>";
    }
    else {
        $currentCell = $currentCell.join '', "$node</td>";
    }

    if ( defined $members{$node}[1] )
    {
        push @queue, $members{$node}[1];
    }
    if ( defined $members{$node}[2] )
    {
        push @queue, $members{$node}[2];
    }
}

$currentCell = $currentCell.join '', "</tr>";
push @htmlTable, $currentCell;
push @htmlTable, "</table>";

my $outputFileName = "tree.html";
open my $outputFileHandle, '>', $outputFileName or die "Could not open file '$outputFileName' $!";

print $outputFileHandle "<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"utf-8\">\n<style>\ntable, th, td { border: 1px solid black; }\ntd { text-align: center; vertical-align:middle; }\n</style>\n</head>\n<body>\n";
foreach( @htmlTable ) {
    print $outputFileHandle "$_\n"; 
}
print $outputFileHandle "</body>\n</html>";
close $outputFileHandle;